﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class genericBackTime : MonoBehaviour {

    public bool isRewinding = false;

    List<Vector2> positions;
    List<Vector2> velocities;
    Rigidbody2D rb;
    List<RigidbodyType2D> rbBodyType;


    // Use this for initialization
    void Start()
    {
        positions = new List<Vector2>();
        velocities = new List<Vector2>();
        rb = GetComponent<Rigidbody2D>();
        rbBodyType = new List<RigidbodyType2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            StartRewind();
        if (Input.GetKeyUp(KeyCode.C))
            StopRewind();
    }


    void FixedUpdate()
    {
        if (isRewinding)
            Rewind();
        else
            Record();
    }

    void Record()
    {
        if (positions.Count >= /*Mathf.Round(5f / Time.fixedDeltaTime)*/300f)
        {
            positions.RemoveAt(positions.Count - 1);
            velocities.RemoveAt(velocities.Count - 1);
            rbBodyType.RemoveAt(rbBodyType.Count - 1);
        }
        positions.Insert(0, transform.position);
        velocities.Insert(0, rb.velocity);
        rbBodyType.Insert(0, rb.bodyType);
    }

    void Rewind()
    {
        if (positions.Count > 0)
        {
            transform.position = positions[0];
            positions.RemoveAt(0);

            rb.velocity = velocities[0];
            velocities.RemoveAt(0);

            rb.bodyType = rbBodyType[0];
            rbBodyType.RemoveAt(0);
        }
        else
        {
            StopRewind();
        }
    }

    public void StartRewind()
    {
        isRewinding = true;
        rb.simulated = false;
    }

    public void StopRewind()
    {
        isRewinding = false;
        rb.simulated = true;
    }
}
