﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class heroMovement : MonoBehaviour {

    [Range(1, 50)]
    public float jumpVelocity;
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
    public float movementSpeed = 5f;
    public float accelerationAlpha = 0.1f;
    public float brakeAlpha = 0.1f;

    public Vector3 initialPos;

    public int jumps = 2;

    Rigidbody2D rb;

    public ParticleSystem pe;
    public ParticleSystem deathPS;

    public backTime goback;


    public bool isDead = false;

	// Use this for initialization
	void Start () {
        initialPos = transform.position;
        rb = GetComponent<Rigidbody2D>();
        //pe = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        rb.velocity = new Vector2(movementSpeed, rb.velocity.y);

        if (jumps > 0)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                rb.velocity = new Vector2(movementSpeed, jumpVelocity);
                jumps--;
            }

        }
        
        //if (rb.velocity.y < 0)
        //{
        //    rb.velocity += Vector2.up * -Physics2D.gravity.y ;
        //}
        if (rb.velocity.y > 0 && !Input.GetKey(KeyCode.Space))
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
            //if (Input.GetKey(KeyCode.D))
            //{
            //    rb.velocity = new Vector2(Mathf.Lerp(rb.velocity.x, movementSpeed, accelerationAlpha), rb.velocity.y);
            //}
            //else if (Input.GetKey(KeyCode.A))
            //{
            //    rb.velocity = new Vector2(Mathf.Lerp(rb.velocity.x, -movementSpeed, accelerationAlpha), rb.velocity.y);
            //}
            //else
            //{
            //    rb.velocity = new Vector2(Mathf.Lerp(rb.velocity.x, 0, brakeAlpha), rb.velocity.y);
            //}

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("soil"))
        {
            pe.Play();
            jumps = 2;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("spike"))
        {
            StartCoroutine(Death());
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("soil") && rb.velocity.x <= 0)
        {
            StartCoroutine(Death());
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("soil"))
        {
            pe.Stop();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("void"))
        {
            StartCoroutine(Death());
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("loadlvl"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else if (collision.gameObject.layer == LayerMask.NameToLayer("loadmenu"))
        {
            SceneManager.LoadScene("mainMenu", LoadSceneMode.Single);
        }
    }

    IEnumerator Death()
    {
        movementSpeed = 0;
        deathPS.Play();
        pe.Stop();
        goback.canGoBack = false;
        GetComponent<MeshFilter>().mesh = null;
        yield return new WaitForSeconds(1f);
        PlayerPrefs.SetString("lastLoadedScene", SceneManager.GetActiveScene().name);
        Debug.Log(PlayerPrefs.GetString("lastLoadedScene"));
        SceneManager.LoadScene("DeadScene");
    }
}
