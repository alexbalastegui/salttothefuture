﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class deadMenu : MonoBehaviour {


    public void PlayGame()
    {
        Debug.Log("GAME");
        //SceneManager.LoadScene("--", LoadSceneMode.Single);
        string sceneName = PlayerPrefs.GetString("lastLoadedScene");
        SceneManager.LoadScene(sceneName);//back to previous scene1?
    }

    public void QuitGame()
    {
        SceneManager.LoadScene("mainMenu", LoadSceneMode.Single);
    }
  
}
