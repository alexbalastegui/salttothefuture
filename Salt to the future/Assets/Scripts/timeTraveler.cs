﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timeTraveler : MonoBehaviour {

    public List<timeParent> mutableObject;

    public KeyCode timeTravelInput = KeyCode.R;
    public float spaceOffset = 50;
    public float correctionStep = 5f;

	// Use this for initialization
	void Start () {
		foreach (timeParent t in FindObjectsOfType<timeParent>())
        {
            t.spaceOffset = spaceOffset;
            mutableObject.Add(t);
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(timeTravelInput))
        {

            Vector3 newPlayerPos = mutableObject[0].present ? new Vector3(transform.position.x, transform.position.y - spaceOffset, transform.position.z) 
                : new Vector3(transform.position.x, transform.position.y + spaceOffset, transform.position.z);

            GameObject collidingObject = null;

            Debug.Log("Targ " + (newPlayerPos + new Vector3(0, spaceOffset, 0)));

            foreach (timeParent t in mutableObject.ToArray())
            {
                t.present = !t.present;
                collidingObject = t.gameObject;
            }

            //Debug.Log(mutableObject[0].present);

            if (collidingObject != null){
                bool validLoc = false;
                Vector3 newPlayerPosRight;
                Vector3 newPlayerPosLeft;
                Vector3 newPlayerPosUp = newPlayerPosRight = newPlayerPosLeft = newPlayerPos;

                while (!validLoc)
                {
                    newPlayerPosUp = newPlayerPos + new Vector3(0, correctionStep, 0);
                    if (collidingObject.GetComponent<Collider2D>().bounds.Contains(newPlayerPosUp))
                    {
                        validLoc = true;
                        newPlayerPos = newPlayerPosUp;
                    }
                    newPlayerPosLeft = newPlayerPos + new Vector3(-correctionStep, 0, 0);
                    if (collidingObject.GetComponent<Collider2D>().bounds.Contains(newPlayerPosLeft))
                    {
                        validLoc = true;
                        newPlayerPos = newPlayerPosLeft;
                    }
                    newPlayerPosRight = newPlayerPos + new Vector3(correctionStep, 0, 0);
                    if (collidingObject.GetComponent<Collider2D>().bounds.Contains(newPlayerPosRight))
                    {
                        validLoc = true;
                        newPlayerPos = newPlayerPosRight;
                    }
                }
            }

            teleport(newPlayerPos);

        }
	}

    void teleport(Vector3 newPos)
    {
        transform.position = newPos;
    }

    bool isWithin2D(Collider2D col, Vector3 spot)
    {
        return (spot.x < col.bounds.center.x - col.bounds.extents.x && spot.x > col.bounds.center.x + col.bounds.extents.x) && 
            (spot.y < col.bounds.center.y - col.bounds.extents.y && spot.y > col.bounds.center.y + col.bounds.extents.y);
    }
}
