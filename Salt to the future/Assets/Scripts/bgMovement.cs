﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bgMovement : MonoBehaviour {

    Transform initPos;
    Rigidbody2D rb;
    float speed = 20f;
	// Use this for initialization
	void Start () {
        initPos = transform;
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        //rb.velocity = new Vector2(speed, rb.velocity.y);
        rb.velocity = new Vector2(Mathf.Sin(Time.deltaTime* speed), Mathf.Sin(Time.deltaTime));
	}
}
