﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timeParent : MonoBehaviour {
    //This script should be attached to the objects in the present and the PARENT field should be filled with 
    public GameObject parent;
    public bool present = true;
    public float spaceOffset = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (!present)
        {
            transform.position = parent.transform.position + new Vector3(0, spaceOffset, 0);
            transform.rotation = parent.transform.rotation;
            transform.localScale = parent.transform.localScale;
        }
	}
}
