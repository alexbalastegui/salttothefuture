﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour {

    public Transform hero;
    public float followAlpha;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(
            Mathf.Lerp(transform.position.x, hero.transform.position.x, followAlpha),
             transform.position.y/*Mathf.Lerp(transform.position.y, hero.transform.position.y, followAlpha)*/,
            transform.position.z);
	}
}
