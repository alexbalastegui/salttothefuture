﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;

public class backTime : MonoBehaviour {

    public bool isRewinding = false;

    List<Vector2> positions;
    List<Vector2> velocities;
    List<int> jumps;
    Rigidbody2D rb;

    public VideoPlayer rewind_video;

    [SerializeField] private backTimeBar rewindBar;

    float energy;
    float maxEnergy = 100f;

    public bool canGoBack = true;

    heroMovement player;


    // Use this for initialization
    void Start ()
    {
        player = GameObject.Find("Player").GetComponent<heroMovement>();
        positions = new List<Vector2>();
        velocities = new List<Vector2>();
        jumps = new List<int>();
        rb = GetComponent<Rigidbody2D>();
        energy = 0f;
        rewindBar.SetSize(energy);
	}
	
	// Update is called once per frame
	void Update () {
        if (canGoBack)
        {
            if (Input.GetKeyDown(KeyCode.C))
                StartRewind();
            if (Input.GetKeyUp(KeyCode.C))
                StopRewind();
        }
    }


    void FixedUpdate()
    {
        energy = (positions.Count) / 300f;
        rewindBar.SetSize(energy);
        
        if (isRewinding)
            Rewind();
        else
            Record();
    }

    void Record()
    {
        if(positions.Count >= /*Mathf.Round(5f / Time.fixedDeltaTime)*/300f)
        {
            positions.RemoveAt(positions.Count - 1);
            velocities.RemoveAt(velocities.Count - 1);
            jumps.RemoveAt(jumps.Count - 1);
        }
        positions.Insert(0, transform.position);
        velocities.Insert(0, rb.velocity);
        jumps.Insert(0, player.jumps);
    }

    void Rewind()
    {
        if (positions.Count > 0)
        {
            transform.position = positions[0];
            positions.RemoveAt(0);
            rb.velocity = velocities[0];
            velocities.RemoveAt(0);
            player.jumps = jumps[0];
            jumps.RemoveAt(0);
        }
        else
        {
            StopRewind();
        }
    }

    public void StartRewind()
    {
        rewind_video.Play();
        isRewinding = true;
        rb.simulated = false;
    }

    public void StopRewind()
    {
        rewind_video.Stop();
        isRewinding = false;
        rb.simulated = true;
    }
}
