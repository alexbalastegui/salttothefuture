﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class backTimeBar : MonoBehaviour {

    public Transform bar;
	
    public void SetSize(float sizeNormalized)
    {
        bar.localScale = new Vector3(sizeNormalized, 1f);   
    }
}
